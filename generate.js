#!/usr/bin/env node
const packageJson = require('./package.json')
const fs = require('fs')



const extensionsDependenciesFolder = './extensionDependencies/'
const readmeTemplatePath = './README.template.md'
const readmePath = './README.md'



function generateBadge(extensionId) {
	return `[![Latest Release][${extensionId}-version-bagde]][${extensionId}-url]
[![Downloads][${extensionId}-download-badge]][${extensionId}-url]
[![Rating][${extensionId}-rating-bagde]][${extensionId}-url]

[${extensionId}-url]:            https://marketplace.visualstudio.com/items?itemName=${extensionId}
[${extensionId}-version-bagde]:  https://img.shields.io/vscode-marketplace/v/${extensionId}.svg
[${extensionId}-download-badge]: https://img.shields.io/vscode-marketplace/d/${extensionId}.svg
[${extensionId}-rating-bagde]:   https://img.shields.io/vscode-marketplace/r/${extensionId}.svg
`
}


function injectBadgesInExtensionSubSection(extensionId, content) {
	const lines = content.split('\n');
	lines.splice(2, 0, generateBadge(extensionId))
	return lines.join('\n')
}


function generateExtensionSubSection(basePath, filename) {
	const extensionId = filename.replace('.md', '')

	const filePath = `${basePath}/${filename}`
	const content = fs.readFileSync(filePath).toString()

	const subSection = injectBadgesInExtensionSubSection(extensionId, content)

	return subSection
}


function generateExtensionsDependenciesSection(extensionsDependenciesFolder) {
	const extensionList = fs.readdirSync(extensionsDependenciesFolder)
	const extensionsDependenciesSection = extensionList
		.map(generateExtensionSubSection.bind(this, extensionsDependenciesFolder))
		.join('\n\n')
	return extensionsDependenciesSection
}


function replaceExtensionsDependenciesSection(extensionsDependenciesFolder, content) {
	const section = generateExtensionsDependenciesSection(extensionsDependenciesFolder)
	return content.replace('${extensionsDependencies}', section)
}


function relativeToAbsoluteUrl({ repository: { url } }, content) {
	const repositoryUrl = url.replace(/\.git$/, '')
	return content.replace(/\$\{repositoryUrl\}/g, repositoryUrl)
}



function main() {
	const readmeTemplate = fs.readFileSync(readmeTemplatePath).toString()

	let readmeContent = readmeTemplate
	readmeContent = replaceExtensionsDependenciesSection(extensionsDependenciesFolder, readmeContent)
	readmeContent = relativeToAbsoluteUrl(packageJson, readmeContent)

	fs.writeFileSync(readmePath, readmeContent)
}



main()
