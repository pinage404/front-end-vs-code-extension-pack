# Front End VS Code Extension Pack

Collection of extensions for front end web development frameworks' agnostic



## Extensions Included

${extensionsDependencies}



## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look


[merge-request-url]: ${repositoryUrl}/merge_requests
[issue-url]:         ${repositoryUrl}/issues
