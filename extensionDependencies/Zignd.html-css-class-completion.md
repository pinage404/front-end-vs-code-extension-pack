### IntelliSense for CSS class names in HTML

CSS class name completion for the HTML class attribute based on the definitions found in your workspace

![demo](https://i.imgur.com/5crMfTj.gif)
