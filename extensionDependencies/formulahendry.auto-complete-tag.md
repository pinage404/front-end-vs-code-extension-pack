### Auto Complete Tag

Extension Packs to add close tag and rename paired tag automatically for HTML/XML

![demo](https://github.com/formulahendry/vscode-auto-rename-tag/raw/master/images/usage.gif)
