### CSS Peek

Allow peeking to css ID and class strings as definitions from html files to respective CSS

![CSS Peek Demo](https://raw.githubusercontent.com/pranaygp/vscode-css-peek/master/working.gif)
