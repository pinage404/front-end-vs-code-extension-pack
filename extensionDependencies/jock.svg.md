### SVG

A Powerful SVG Langauge Support Extension

### SVG Elements Auto Completion

![auto completion](https://github.com/lishu/vscode-svg/raw/master/images/f1.png)

### SVG Attributes Auto Completion

![attributes auto completion](https://github.com/lishu/vscode-svg/raw/master/images/f2.png)

### Document Symbol with SVG Element [id]

![document symbol](https://github.com/lishu/vscode-svg/raw/master/images/f3.png)

### SVG Preview

![preview](https://github.com/lishu/vscode-svg/raw/master/images/f4.png)
